﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : Singleton<PauseMenu> {

    public static bool paused;
    public GameObject blackScreen;

public static void Pause(bool state)
    {
        paused = state;
        Instance.blackScreen.SetActive(state);

        if (paused)
        {
            if (!PhotonNetwork.inRoom)
                Time.timeScale = 0;
            UnityEngine.Cursor.lockState = CursorLockMode.None;
            UnityEngine.Cursor.visible = true;
        }
        else
        {
            if (!PhotonNetwork.inRoom)
                Time.timeScale = 1;
            UnityEngine.Cursor.lockState = CursorLockMode.Locked;
            UnityEngine.Cursor.visible = false;
        }
    }
}

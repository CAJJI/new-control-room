﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour {

    public List<GameObject> detections = new List<GameObject>();
    public List<Object> detectables = new List<Object>();

    public float radius;
    public Vector3 offset;

    public Trigger updated = new Trigger();

    private void Start()
    {
        SphereCollider collider = GetComponent<SphereCollider>();
        if (collider)
        {
            collider.enabled = false;
            radius = collider.radius;
            offset = collider.center;
        }
    }

    RaycastHit hit;
    public bool CanSee(Vector3 start, Vector3 end, GameObject target, LayerMask layerMask)
    {        
        if (Physics.Raycast(start, (end - start).normalized,out hit, 50, layerMask))
        {
            if (hit.collider.gameObject == target) return true;
            float distance = (end - start).magnitude;
            if (hit.distance > distance) return true;
        }
        return false;
    }    

    //public void UpdateDetections()
    //{        
    //    Collider[] colliders = Physics.OverlapSphere(transform.position + transform.rotation * offset, radius);
    //    List<GameObject> check = new List<GameObject>();
    //    foreach(Collider other in colliders)
    //    {
    //        check.Add(other.gameObject);
    //        foreach (Object detect in detectables)
    //        {
    //            if (other.GetComponent(detect.name.ToString()) && !detections.Contains(other.gameObject))
    //                detections.Add(other.gameObject);
    //        }
    //    }
    //    for (int i = 0; i < detections.Count; i++)
    //    {
    //        if (!check.Contains(detections[i]))
    //        {
    //            detections.Remove(detections[i]);
    //        }
    //    }
    //}

    public bool Contains<T>()
    {
        foreach(GameObject detection in detections)
        {
            if (detection != null && detection.GetComponentInParent<T>() != null)
                return true;
        }
        return false;
    }

    void LateUpdate()
    {
        for (int i = 0; i < detections.Count; i++)
        {
            if (!detections[i]) detections.Remove(detections[i]);
        }
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        foreach(Object detect in detectables)
        {
            if (other.GetComponent(detect.name.ToString()) && !detections.Contains(other.gameObject))
                detections.Add(other.gameObject);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (detections.Contains(other.gameObject))
            detections.Remove(other.gameObject);
    }
    */
}

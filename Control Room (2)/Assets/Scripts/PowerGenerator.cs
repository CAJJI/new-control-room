﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerGenerator : Character {

    public bool disabled;

    public List<Power> powers;
    public List<Power> backUpPowers;

    public WorldButton repairButton;
    public GameObject repairLight;
    public Color onColor;
    public Color offColor;
    public AudioClip generatorClip;
    public AudioClip switchClip;
    public AudioClip backUpClip;

    private void Start()
    {
        DetectionManager.AddCharacter(this);
        DetectionManager.AddDetectable(gameObject);
        SetButtonColor(onColor);
        if (startDead)
            Die(new DamageInfo(1000, Vector3.zero, this, GetComponentInChildren<Limb>()));
    }    

    [PunRPC]
    public override void RPCDamage(int characterId, float damage, Vector3 direction, int limbId)
    {
        DamageInfo info = DamageInfo.GetInfo(this, characterId, damage, direction, limbId);
        if (dead) return;
        health -= info.damage;

        if (health <= 0)
        {
            health = 0;
            Die(info);
        }
    }

    [PunRPC]
    public override void RPCDie(int characterId, float damage, Vector3 direction, int limbId)
    {
        if (dead) return;
        health = 0;
        dead = true;
        audioSource.Stop();
        SetPower(false);
        Player player = Player.Instance;
        if (player)
            player.audioSource.volume += 0.3f;
    }    

    //public void Repair(float amount)
    public void Repair()
    {        
        if (health > 0) return;
     
        health += 100;
        if (health >= 100)
        {
            disabled = false;
            dead = false;
            StartCoroutine(StartPower());
            health = 100;
            audioSource.Play();                  
        }
    }

    public void SetButtonColor(Color color)
    {
        repairButton.GetComponent<Renderer>().material.color = color;
        repairButton.GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
        repairLight.GetComponent<Renderer>().material.color = color;
        repairLight.GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
    }

    public void SetPower(bool state)
    {
        Debug.Log(state);
        disabled = !state;         
        Manager.CreateAudio(transform.position, switchClip, Vector2.one, 0.5f);        
        foreach (Power power in powers)
        {
            power.Enable(state);
        }
        if (state)
        {
            SetButtonColor(onColor);
            SetBackUpPower(false);
        }
        else
        {
            SetButtonColor(offColor);
            StartCoroutine(StartBackUpPower());
        }
    }

    public void SetBackUpPower(bool state)
    {
        Manager.CreateAudio(transform.position, switchClip, Vector2.one, 0.5f);
        Manager.PlayAudio(audioSource, backUpClip);
        foreach (Power backUpPower in backUpPowers)
        {
            backUpPower.Enable(state);
        }
    }

    public IEnumerator StartPower()
    {
        SetBackUpPower(false);
        SetButtonColor((onColor + offColor) / 2);
        yield return new WaitForSeconds(1);
        if (!disabled)
            SetPower(true);
    }

    public IEnumerator StartBackUpPower()
    {
        yield return new WaitForSeconds(3);
        if (disabled)
            SetBackUpPower(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMarkerUI : MonoBehaviour {

    private void Awake()
    {
        Hide(true);
    }

    public void Hide(bool state) {
        gameObject.SetActive(!state);        
    }

    public void SetPosition(Vector3 position) {
        Hide(false);
        position.y = 9;
        transform.position = position;
    }
}

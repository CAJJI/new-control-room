﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item {

    public LayerMask damageLayerMask;
    public DamageInfo damageInfo;
    public float attackDelay;
    public float attackTime;
    public bool attacking;
    public Collider attackCollider;
    public GameObject fleshParticle;
    public GameObject metalParticle;
    public List<Damagable> hitDamagables;
    public List<Character> hitCharacters;

    public AudioSource audioSource;
    public AudioClip fireClip;
    public AudioClip hitMetalClip;
    public AudioClip hitFleshClip;    

    [PunRPC]
    public override void RPCUse()
    {        
        Manager.CreateAudio(transform.position, fireClip, new Vector2(0.8f, 1.2f));
        StartCoroutine("Swing", attackTime);
        SetAttackTime();
    }

    public void SetAttackTime()
    {
        attackTime = Time.timeSinceLevelLoad;
    }

    public IEnumerator Swing(float time)
    {
        if (!attacking)
        {
            attacking = true;
            hitCharacters.Clear();
            hitDamagables.Clear();
            float currentTime = 0;
            attackCollider.enabled = true;
            while (currentTime < time && attacking)
            {
                currentTime++;
                yield return null;
            }
            attackCollider.enabled = false;
            attacking = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (attacking)
        {
            Damagable damagable = other.GetComponent<Damagable>();
            if (damagable && !hitDamagables.Contains(damagable))
            {
                Character character = damagable.GetComponentInParent<Character>();
                if (character && hitCharacters.Contains(character)) return;

                if (!CanDamage(damagable.damageMasks)) return;

                Limb limb = damagable.GetComponent<Limb>();
                if (limb && limb.character == holder) return;                
                if (holder)
                {
                    damageInfo.direction = holder.transform.forward;
                    damageInfo.attacker = holder;
                }
                else
                {
                    damageInfo.direction = (other.transform.position - transform.position);
                }
                if (!PhotonNetwork.inRoom || (holder.GetComponent<Player>() && holder.photonView.isMine) || (!holder.GetComponent<Player>()))
                damagable.Damage(damageInfo);
                attacking = false;
                hitDamagables.Add(damagable);

                if (other.gameObject.layer == LayerMask.NameToLayer("Environment") || !damagable)
                {
                    Manager.CreateAudio(transform.position, hitMetalClip, new Vector2(0.8f, 1.2f), 1, 30);
                    Manager.CreateParticle(ParticleType.Metal, transform.position, Quaternion.LookRotation(-transform.forward));
                    //Instantiate(metalParticle, transform.position, Quaternion.LookRotation(-transform.forward));
                }
                else
                {                    
                    Manager.CreateAudio(transform.position, hitFleshClip, new Vector2(0.8f, 1.2f), 1, 30);
                    Manager.CreateParticle(ParticleType.Flesh, transform.position, Quaternion.LookRotation(-transform.forward));
                    //Instantiate(fleshParticle, transform.position, Quaternion.LookRotation(-transform.forward));
                }
                //attacking = false;
            }
        }
    }

    public bool CanDamage(List<DamageMask> masks)
    {
        //if (PhotonNetwork.inRoom && !photonView.isMine) return false;
        bool cantDamage = true;
        foreach (DamageMask mask in masks)
        {
            if (damageInfo.damageMasks.Contains(mask))
                cantDamage = false;
        }
        return !cantDamage;
    }

    public override bool CanUse()
    {
        if (Time.timeSinceLevelLoad - attackTime > attackDelay)
            return true;
        return false;
    }
}

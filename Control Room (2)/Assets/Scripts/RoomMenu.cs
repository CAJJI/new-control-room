﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class RoomMenu : Singleton<RoomMenu> {

    public PhotonView photonView;
    public ConnectionWindow errorWindow;
    public LobbyMenu lobbyMenu;
    public Dropdown mapDropDown;
    public Dropdown difficultyDropDown;
    public Image mapLayoutImage;
    public GameObject startButton;
    public GameObject backButton;
    public GameObject spectateButton;
    public Text playerCountText;
    public Text countDownText;
    public GameObject settings;
        
    public PlayerDropDown playerDropDown;    

    public List<PlayerDropDown> playersInRoom;

    public Transform dropDownPosition;
    public int verticalLimit;
    public float verticalOffset;
    public float horizontalOffset;
    public bool connectedToRoom;


    public void SetMap(int dropDownIndex)
    {        
        int i = (int)Level.Factor + dropDownIndex;
        LevelInfo info = GlobalManager.GameDef.GetLevel((Level)i);
        GlobalManager.levelInfo.level = info.level;
        GlobalManager.levelInfo.scene = info.scene;
        if (mapDropDown.value != dropDownIndex)
            mapDropDown.value = dropDownIndex;
        mapLayoutImage.sprite = info.preview;        
        UpdateSettings(PhotonTargets.Others);        
        MultiplayerManager.UpdateCustomProperty(PhotonNetwork.room, CustomPropertyType.Map, GlobalManager.levelInfo.level);
    }

    public void SetDifficulty(int index)
    {
        GlobalManager.levelInfo.difficulty = index;
        difficultyDropDown.value = index;
        UpdateSettings(PhotonTargets.Others);
        MultiplayerManager.UpdateCustomProperty(PhotonNetwork.room, CustomPropertyType.Difficulty, GlobalManager.levelInfo.difficulty);
    }

    private void OnEnable()
    {
        settings.SetActive(false);
        spectateButton.SetActive(false);
        mapDropDown.ClearOptions();
        List<string> maps = new List<string>();
        for (int i = (int)Level.Factor; i < (int)Level.Crest + 1; i++)
        {
            maps.Add((System.Enum.GetName(typeof(Level), i)));
        }
        mapDropDown.AddOptions(maps);
        //SetMap(0);
        mapDropDown.onValueChanged.AddListener(SetMap);        

        difficultyDropDown.ClearOptions();
        List<string> difficulties = new List<string>();
        for (int i = (int)0; i < (int)Difficulty.Insane + 1; i++)
        {
            difficulties.Add((System.Enum.GetName(typeof(Difficulty), i)));

        }
        difficultyDropDown.AddOptions(difficulties);
        //SetDifficulty((int)Difficulty.Medium);
        difficultyDropDown.onValueChanged.AddListener(SetDifficulty);        

        StartJoinRoom();        
    }

    void StartJoinRoom()
    {
        countDownText.text = "";
        startButton.SetActive(false);
        backButton.SetActive(false);
        foreach (PlayerDropDown dropDown in playersInRoom)
        {
            if (dropDown.gameObject)
                Destroy(dropDown.gameObject);
            else playersInRoom.Remove(dropDown);
        }
        playersInRoom = new List<PlayerDropDown>();
        connectedToRoom = false;
        StartCoroutine("JoinRoom");
    }

    IEnumerator JoinRoom()
    {
        float timeout = 0;
        bool disconnect = false;
        while (!PhotonNetwork.inRoom && !disconnect)
        {
            timeout += Time.deltaTime;
            if (timeout > 5) disconnect = true;            
            yield return null;
        }
        if (disconnect || !PhotonNetwork.inRoom)
        {
            MenuManager.Instance.DisconnectFromLobby();
        }
        else if ((bool)MultiplayerManager.GetCustomProperty(PhotonNetwork.room, CustomPropertyType.Starting))
            Disconnect("Game is starting, please try again to spectate.");
        else
        {
            MultiplayerManager.activeRoom = PhotonNetwork.room;
            connectedToRoom = true;
            //if (PhotonNetwork.isMasterClient)
            //{
            //    MultiplayerManager.UpdateCustomProperty(PhotonNetwork.room, CustomPropertyType.Starting, false);
            //}
            backButton.SetActive(true);
            UpdatePlayers();
            CheckSpectate();
        }
    }    

    private void Update()
    {        
        if (!connectedToRoom) return;
        if (!PhotonNetwork.inRoom)
        {
            Disconnect("Disconnected.");
        }
        else
        {
            //startButton.SetActive(PhotonNetwork.isMasterClient && PhotonNetwork.room.IsVisible);            
            //mapDropDown.interactable = PhotonNetwork.isMasterClient && PhotonNetwork.room.IsVisible;
            //difficultyDropDown.interactable = PhotonNetwork.isMasterClient && PhotonNetwork.room.IsVisible;

            //change to check if room started
            bool starting = (bool)MultiplayerManager.GetCustomProperty(PhotonNetwork.room, CustomPropertyType.Starting);
            startButton.SetActive(PhotonNetwork.isMasterClient && !starting);
            mapDropDown.interactable = (PhotonNetwork.isMasterClient && !starting);
            difficultyDropDown.interactable = (PhotonNetwork.isMasterClient && !starting);
        }
    }

    void CheckSpectate()
    {
        if ((bool)MultiplayerManager.GetCustomProperty(PhotonNetwork.room, CustomPropertyType.InGame))
        {
            //spectateButton.SetActive(true);
        }
        else
        {
            //spectateButton.SetActive(false);
        }
    }

    public void Spectate()
    {
        MultiplayerManager.SetupRoom();
        MultiplayerManager.spectatingOutside = true;
        LevelInfo levelInfo = GlobalManager.GameDef.GetLevel(GlobalManager.levelInfo.level);
        SceneManager.LoadScene(levelInfo.scene);
    }

    public void RejoinRoom()
    {
        if (!PhotonNetwork.inRoom)
        {
            if (GlobalManager.currentRoom.PlayerCount >= GlobalManager.currentRoom.MaxPlayers)
            {
                Disconnect("Room is full.");
                return;
            }

            if (!MultiplayerManager.RoomExists(GlobalManager.currentRoom.Name))
            {                
                Disconnect("Room no longer exists.");
                return;
            }
            PhotonNetwork.JoinRoom(GlobalManager.currentRoom.Name);
        }

        MenuManager.Instance.OpenMenu(gameObject);        
    }

    public void StartGame()
    {                
        if (!PhotonNetwork.isMasterClient) return;
        MultiplayerManager.UpdateCustomProperty(PhotonNetwork.room, CustomPropertyType.Starting, true);
        LevelInfo info = GlobalManager.levelInfo;
        photonView.RPC("RPCAssignSettings", PhotonTargets.AllBufferedViaServer, info.difficulty, (int)info.level, info.scene);
        photonView.RPC("RPCStartGame", PhotonTargets.AllViaServer, info.difficulty, (int)info.level, info.scene);
    }

    [PunRPC]
    public void RPCAssignSettings(int difficulty, int level, int scene)
    {
        LevelInfo info = new LevelInfo();
        info.difficulty = difficulty;
        info.level = (Level)level;
        info.scene = scene;
        GlobalManager.levelInfo = info;
    }

    [PunRPC]
    public void RPCStartGame(int difficulty, int level, int scene)
    {
        LevelInfo info = new LevelInfo();
        info.difficulty = difficulty;
        info.level = (Level)level;
        info.scene = scene;
        GlobalManager.levelInfo = info;
        startButton.SetActive(false);
        backButton.SetActive(false);
        StartCoroutine(CountDown());        
    }

    IEnumerator CountDown(){

        //PhotonNetwork.room.IsVisible = false;
        float countDown = 5;
        while (countDown > 0) {
            countDownText.text = Mathf.CeilToInt(countDown).ToString();
            countDown -= Time.deltaTime;            
            yield return null;
        }

        MultiplayerManager.SetupRoom();
        LevelInfo levelInfo = GlobalManager.GameDef.GetLevel(GlobalManager.levelInfo.level);
        SceneManager.LoadScene(levelInfo.scene);
    }
    

    public static void Kick(PhotonPlayer player)
    {
        Instance.photonView.RPC("RPCKick", PhotonTargets.AllViaServer, player);
    }

    [PunRPC]
    public void RPCKick(PhotonPlayer player)
    {
        if (player == PhotonNetwork.player)
        {
            Disconnect("Kicked by host.");
        }
    }

    void OnPhotonPlayerDisconnected(PhotonPlayer other)
    {        
        if (PhotonNetwork.isMasterClient)
        UpdatePlayers();
    }

    public void UpdatePlayers()
    {
        photonView.RPC("PUNUpdatePlayers", PhotonTargets.AllViaServer);
    }

    public void Disconnect()
    {
        Disconnect("");
    }
    
    public void Disconnect(string message)
    {
        PhotonNetwork.LeaveRoom();
        if (message != "")
        {
            errorWindow.GetComponentInChildren<Text>().text = message;
            MenuManager.Instance.OpenMenu(errorWindow.gameObject);
            errorWindow.Disconnect();
        }
        else
        {
            MenuManager.Instance.OpenMenu(lobbyMenu.gameObject);            
        }
        //lobbyMenu.GenerateList();
    }

    void UpdatePlayerCount()
    {
        playerCountText.text = PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers;
    }

    [PunRPC]
    public void PUNUpdatePlayers()
    {
        UpdatePlayerCount();
        PhotonPlayer[] players = PhotonNetwork.playerList;

        for (int i = 0; i < playersInRoom.Count; i++)
        {
            Destroy(playersInRoom[i].gameObject);            
        }

        playersInRoom = new List<PlayerDropDown>();

        for (int i = 0; i < players.Length; i++)
        {            
            PhotonPlayer player = players[i];            
                PlayerDropDown dropDown = Instantiate(playerDropDown.gameObject, dropDownPosition.position, Quaternion.identity, dropDownPosition).GetComponent<PlayerDropDown>();
                dropDown.Assign(player);
                playersInRoom.Add(dropDown);             
            
        }

        for (int i = 0; i < playersInRoom.Count; i++)
        {

            PlayerDropDown dropDown = playersInRoom[i];
            Vector2 offset = Vector2.down * verticalOffset * (i % verticalLimit);
            if (i >= verticalLimit)
                offset += Vector2.right * horizontalOffset;
            dropDown.transform.position = dropDownPosition.position;
            dropDown.GetComponent<RectTransform>().anchoredPosition += offset;
        }

        UpdateSettings(PhotonTargets.All);
    }

    public void UpdateSettings(PhotonTargets targets)
    {        
        if (PhotonNetwork.isMasterClient)
        {            
            LevelInfo info = GlobalManager.levelInfo;
            photonView.RPC("RPCUpdateSettings", targets, info.difficulty, (int)info.level, info.scene, GlobalManager.Instance.gameVersion);
        }
    }

    [PunRPC]
    public void RPCUpdateSettings(int difficulty, int level, int scene, float gameVersion)
    {
        if (GlobalManager.Instance.gameVersion != gameVersion)
        {
            Disconnect("Room was created using a different version of the game.");
            return;
        }
        settings.SetActive(true);
        SetDifficulty(difficulty);
        int dropDownIndex = level - (int)Level.Factor;        
        SetMap(dropDownIndex);
    }

    public bool PlayerInRoom(PhotonPlayer player)
    {
        PhotonPlayer[] players = PhotonNetwork.playerList;
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] == player) return true;
        }
        return false;
    }

    public bool ContainsPlayer(PhotonPlayer player)
    {
        foreach(PlayerDropDown playerDropDown in playersInRoom)
        {
            if (playerDropDown.player == player)
                return true;
        }
        return false;
    }
}

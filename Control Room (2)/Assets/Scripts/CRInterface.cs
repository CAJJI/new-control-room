﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRInterface : Interactable {

    static CRInterface _instance;
    public static CRInterface Instance { get { if (_instance == null) _instance = GameObject.FindObjectOfType<CRInterface>(); return _instance; } }

    public MapMarkerUI[] mapMarkers;

    RaycastHit currentHit;    
    public TextMesh log;
    public Camera mapLayout;
    public LayerMask mapLayerMask;
    public GameObject cameraRenderer;
    public GameObject viewCameraPrefab;
    public Camera viewCamera;
    public CameraInterface currentCamera;

    public InputHold mouseHold = new InputHold();

    MilitaryUnit selectedUnit;
    public GameObject unitSelectionMenuPrefab;
    GameObject unitSelectMenu;
    WorldButton selectedButton;

    public UnitStatBarMenu unitStatBarMenu;

    public AudioSource audioSource;
    public AudioClip clickClip;
    public AudioClip popUpClip;

    float timeDisplayDelay;

    public bool transmissionDown;

    private void Start()
    {
        selectedUnit = null;
        if (!currentCamera) currentCamera = GameObject.FindObjectsOfType<CameraInterface>()[0];
        Debug.Log(GameObject.FindObjectsOfType<CameraInterface>().Length.ToString() + currentCamera);
        SelectCamera(currentCamera, false);

        for (int i = 0; i < mapMarkers.Length; i++) {
            MilitaryUnit militaryUnit = MilitaryManager.GetUnit((UnitCode)i);
            mapMarkers[i].GetComponent<Renderer>().material.color = militaryUnit.color;
        }
    }

    private void Update()
    {
        MouseInput();
        TimeDisplay();
        Highlights();
        CameraPosition();
    }

    private void LateUpdate()
    {
        //Doesn't rotate up and down
        //CameraPosition();
    }

    void Highlights()
    {
        if (Player.isDead) return;
        Player player = Player.Instance;        
        RaycastHit hit = player.IsLookingAt(10, mapLayerMask);
        if (!hit.collider) return;

        if (hit.collider.gameObject != gameObject) return;

        Vector2 point = hit.textureCoord;
        Ray camRay = mapLayout.ScreenPointToRay(new Vector2(point.x * mapLayout.pixelWidth, point.y * mapLayout.pixelHeight));
        if (Physics.Raycast(camRay, out hit, 100, mapLayerMask))
        {
            WorldButton button = hit.collider.GetComponent<WorldButton>();
            if (button)
            {
                button.highlight.state = true;
            }
        }
    }

    void TimeDisplay()
    {
        if (!Manager.lockDown) return;
        if (transmissionDown) return;
        timeDisplayDelay++;
        if (timeDisplayDelay % 5 != 0) return;
        Vector3 timeVector = Manager.timeVector;        
        LogText(String.Format("{0:00}:{1:00}", timeVector.x, timeVector.y));
        if (Manager.timeRemaining <= 0)
            LogText("RESCUE ARRIVED. REACH EXIT.");
        timeDisplayDelay = 0;
    }

    void MouseInput()
    {
        if (mouseHold)
        {
            if (!Input.GetMouseButton(0)) mouseHold.Release();
            if (mouseHold.timePassed > 0.3f)
            {
                if (!unitSelectMenu) {
                    if (!GetHit(currentHit).collider) return;
                    Vector3 position = currentHit.point;                    
                    position += (transform.forward * 0.09f);
                    unitSelectMenu = Instantiate(unitSelectionMenuPrefab, position, transform.rotation);
                    unitSelectMenu.GetComponent<UnitSelectionMenu>().CRinterface = this;
                    Manager.PlayAudio(audioSource, popUpClip);
                }
            }
            else
            {
                if (!Input.GetMouseButton(0))
                {
                    RaycastHit hit = Player.Instance.IsLookingAt(10, mapLayerMask);
                    {
                        SelectPoint(hit);
                    }
                    mouseHold.Release();                    
                }
            }            
        }
        else
        {            
            if (unitSelectMenu != null)
            {                
                UnitSelectionMenu menu = unitSelectMenu.GetComponent<UnitSelectionMenu>();
                if (menu.selectedButton != 0) {
                    if (PhotonNetwork.inRoom) {
                        SetMarker(menu.selectedButton - 1, GetPoint(currentHit));
                    } 
                    else {
                        SelectUnitDestination(menu.selectedButton - 1);
                    }
                }
                menu.Close();                
                Manager.PlayAudio(audioSource, clickClip);                
            }
        }
    }

    public void InteractWithDoor(BlastDoor door)
    {
        door.Interact();
    }

    public void SelectCamera(CameraInterface cameraInterface)
    {
        SelectCamera(cameraInterface, false);
    }

    public void SelectCamera(CameraInterface cameraInterface, bool andClick, bool RPC = true)
    {
        if (PhotonNetwork.inRoom && RPC)
            photonView.RPC("RPCSelectCamera", PhotonTargets.AllBufferedViaServer, cameraInterface.id, andClick);
        else
            RPCSelectCamera(cameraInterface.id, andClick);
    }

    [PunRPC]
    public void RPCSelectCamera(int id, bool andClick)
    {
        if (andClick) Manager.PlayAudio(audioSource, clickClip);
        if (currentCamera) currentCamera.camera = null;
        currentCamera = Manager.Instance.GetBehaviour<CameraInterface>(id);
        currentCamera.camera = viewCamera;
        viewCamera.transform.position = currentCamera.camPosition.position;
        viewCamera.transform.rotation = currentCamera.camPosition.rotation;
        viewCamera.transform.SetParent(currentCamera.camPosition);
    }   

    void CameraPosition() {
        if (!viewCamera)
        {
            viewCamera = Instantiate(viewCameraPrefab, transform.position, transform.rotation).GetComponent<Camera>();
            SelectCamera(FindObjectOfType<CameraInterface>(), false, false);            
        }
        if (currentCamera)
        {
            //viewCamera.transform.position = currentCamera.camPosition.position;
            //viewCamera.transform.rotation = currentCamera.camPosition.rotation;
            //viewCamera.transform.forward = currentCamera.camPosition.forward;
        }
    }

    public void ViewPlayerInfo(Player player) {
        if (PhotonNetwork.inRoom) {
            photonView.RPC("RPCViewPlayerInfo", PhotonTargets.AllBuffered, player.id);
        } else {
            RPCViewPlayerInfo(player.id);
        }
    }

    [PunRPC]
    public void RPCViewPlayerInfo(int playerId) {
        Player player = Manager.Instance.GetBehaviour<Player>(playerId);        
        unitStatBarMenu.AssignPlayer(player);
        Manager.PlayAudio(audioSource, clickClip);
    }

    public void ViewUnitInfo(int unitCode)
    {
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCViewUnitInfo", PhotonTargets.AllBufferedViaServer, unitCode);
        else
            RPCViewUnitInfo(unitCode);
            
    }

    [PunRPC]
    public void RPCViewUnitInfo(int unitCode)
    {
        unitStatBarMenu.AssignUnit(MilitaryManager.GetUnit((UnitCode)unitCode));
        Manager.PlayAudio(audioSource, clickClip);
    }

    public void SetMarker(int index, Vector3 position) {
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCSetMarker", PhotonTargets.AllBuffered, index, position);
        else
            RPCSetMarker(index, position);
    }

    [PunRPC]
    public void RPCSetMarker(int index, Vector3 position) {
        Debug.Log("Set Marker");
        mapMarkers[index].SetPosition(position);
    }

    public void SelectUnitDestination(int unitCode)
    {        
        selectedUnit = MilitaryManager.GetUnit((UnitCode)unitCode);                
        SelectPoint(currentHit);
        
    }

    [PunRPC]
    public override void RPCInteract(int characterId)
    {
        Character character = Manager.Instance.GetBehaviour<Character>(characterId);
        currentHit = character.GetComponent<Player>().IsLookingAt(10, mapLayerMask);
        mouseHold.Press();        
    }

    RaycastHit GetHit(RaycastHit hit)
    {
        Vector2 point = hit.textureCoord;
        Ray camRay = mapLayout.ScreenPointToRay(new Vector2(point.x * mapLayout.pixelWidth, point.y * mapLayout.pixelHeight));        
        Physics.Raycast(camRay, out hit, 100, mapLayerMask);
        return hit;            
    }

    Vector3 GetPoint(RaycastHit hit) {
        Vector2 point = hit.textureCoord;
        Ray camRay = mapLayout.ScreenPointToRay(new Vector2(point.x * mapLayout.pixelWidth, point.y * mapLayout.pixelHeight));
        return camRay.origin;
    }

    public void SelectPoint(RaycastHit hit)
    {        
        hit = GetHit(hit);
        if (hit.collider)
        {
            if (selectedUnit != null)
            {
                SetUnitDestination(selectedUnit, hit.point);
            }
            else if (mouseHold.timePassed < 0.3f)
            {
                WorldButton button = hit.collider.GetComponent<WorldButton>();
                if (button)
                {
                    button.Event.Invoke();
                }
            }
        }
        selectedUnit = null;
        Manager.PlayAudio(audioSource, clickClip);
    }

    public void SetUnitDestination(MilitaryUnit unit, Vector3 destination)
    {
        unit.AssignPathFormation(Pathfind.GetPath(unit.GetCurrentPosition(), destination));        
    }

    public void LogText(string text)
    {
        log.text = text;
    }
}

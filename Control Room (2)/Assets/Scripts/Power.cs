﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : MonoBehaviour {

    public List<GameObject> onEnable = new List<GameObject>();

    public PowerGenerator generator;
    public bool backUp;

    private void Start()
    {
        if (onEnable.Count == 0 && transform.childCount > 0)
            onEnable.Add(transform.GetChild(0).gameObject);

        if (generator)
        {
            if (backUp)
                generator.backUpPowers.Add(this);
            else
                generator.powers.Add(this);
        }
    }

    public virtual void Enable(bool state)
    {
        foreach(GameObject GO in onEnable)
        {
            GO.SetActive(state);
        }
        foreach(Power power in GetComponentsInChildren<Power>())
        {
            if (power == this) continue;
            power.Enable(state);
        }
    }    
}

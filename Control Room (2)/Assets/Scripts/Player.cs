﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class Player : Character {    

    public bool debug;
    public static bool isDead;

    static Player _Instance;
    public static Player Instance { get { if (_Instance == null) _Instance = GetMyPlayer(); return _Instance; } }

    public GameObject camPivot;    
    public Camera cam;    
    public Vector3 cameraEuler;    
    public float mouseSensitivity;
    public bool zooming;
    public bool attacking;    

    public Interactable activeInteractable;
    public LayerMask interactLayerMask;
    public LayerMask soldierLayerMask;
    
    public ResetBool freezeMovement = new ResetBool();
    public ResetBool freezeCamera = new ResetBool();

    public Light flashLight;

    public float detectTimer;

    public PlayerPositionUI positionUI;
    public PlayerLabel label;

    public bool ghost;

    public static RaycastHit interacterHit;
    public static RaycastHit damageHit;

    public static Player GetMyPlayer()
    {
        foreach(Player player in GameObject.FindObjectsOfType<Player>())
        {
            if (player.photonView.isMine || !PhotonNetwork.inRoom)
                return player;
        }
        return null;
    }

    private void Start()
    {
        damageLayerMask = GlobalManager.GameDef.damageLayerMask;

        RPCFlashLight();
        RPCFlashLight();
        DetectionManager.AddSoldier(detector);
        if (PhotonNetwork.inRoom)
        MultiplayerManager.Instance.players.Add(this);
        if (PhotonNetwork.inRoom) 
        {
            if (photonView.isMine) {
                Color color = Color.red + Color.yellow;
                color *= Random.Range(0.2f, 1f);

                positionUI = PhotonNetwork.Instantiate(positionUI.gameObject.name, transform.position, transform.rotation, 0).GetComponent<PlayerPositionUI>();
                positionUI.AssignPlayer(this, color);        
                positionUI.gameObject.SetActive(false);

                label = PhotonNetwork.Instantiate(GlobalManager.GameDef.playerLabelPrefab.gameObject.name, transform.position, transform.rotation, 0).GetComponent<PlayerLabel>();
                label.AssignPlayer(this, color);                
            }
        }

        if (!debug && photonView && !photonView.isMine && PhotonNetwork.inRoom)
        {
            //Destroy(GetComponentInChildren<Camera>().gameObject);
            GetComponentInChildren<Camera>().gameObject.SetActive(false);
        }

        if (!photonView.isMine && PhotonNetwork.inRoom) return;
        if (!inventory.hud)
        {
            InventoryHUD hud = FindObjectOfType<InventoryHUD>();
            inventory.hud = hud;
            hud.character = this;
        }

        maxHealth = GlobalManager.GameDef.GetCharacterDef(CharacterType.Player).health;
        health = maxHealth;
    }

    LayerMask damageLayerMask;
    void Update()
    {
        if (Manager.debugMode)
        {
            if (Input.GetKeyDown(KeyCode.Alpha4))
                Die(new DamageInfo(100, Vector3.zero, this, GetLimb(1)));
        }
        //DevDebug();        

        damageHit = IsLookingAt(50, damageLayerMask);
        interacterHit = IsLookingAt(5, interactLayerMask);

        UpdateItem();

        if (!debug && photonView && !photonView.isMine && PhotonNetwork.inRoom) return;
        if (PauseMenu.paused && !PhotonNetwork.inRoom) return;

        UpdateCharacter();
        ResetBools();
        Breathing();

        if (PauseMenu.paused && PhotonNetwork.inRoom) return;
        if (GlobalManager.chatting) return;
        Inputs();
    }

    void DevDebug()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            photonView.RPC("RPCSkipGame", PhotonTargets.AllBuffered);
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            Damage(new DamageInfo(100, Vector3.back, this, limbs[0]));
        }
    }

    [PunRPC]
    void RPCSkipGame()
    {
        Manager.Instance.transmissionScreens = new TransmissionScreen[0];
        Manager.timeRemaining = 6;
    }

    void Breathing()
    {
        //if (!detector.updated) return;
        //detectTimer++;
        //if (detectTimer % 10 == 0)
        //detector.UpdateDetections();                

        if (detector.Contains<Infected>() && audioSource.volume < 0.5f)
        {
            audioSource.volume += 0.005f;    
        }
        else if (audioSource.volume > 0)
        {
            audioSource.volume -= 0.002f;            
        }
    }

    void Inputs()
    {
        Movement();
        //Call();
        CameraRotation();
        Interactable();        
        CameraZoom();
        FlashLight();
        InventorySelect();
    }    

    //void Call()
    //{
    //    if (Input.GetKeyDown(KeyCode.E))
    //    {
    //        Collider col = IsLookingAt(50, soldierLayerMask).collider;
    //        if (col && col.GetComponent<Soldier>())
    //            col.GetComponent<Soldier>().Call(this);
    //    }
    //}

    [PunRPC]
    public override void RPCDie(int attackerId, float damage, Vector3 direction, int limbId)
    {
        if (dead) return;
        isDead = true;
        //GlobalManager.Instance.LoadScene(0);
        int id = -1;
        if (PhotonNetwork.inRoom) id = this.id;
        Manager.Instance.StartCoroutine(Manager.Instance.PlayerDeath(id));
        //base.Die(DamageInfo.GetInfo(characterId, damage, direction, limbId));        
        base.RPCDie(attackerId, damage, direction, limbId);
    }    

    [PunRPC]
    public override void RPCDamage(int characterId, float damage, Vector3 direction, int limbId)
    {
        PostProcessingProfile postProcessingProfile = GlobalManager.GameDef.postProcessingProfile;
        //base.RPCDamage(DamageInfo.GetInfo(characterId, damage, direction, limbId));
        base.RPCDamage(characterId, damage, direction, limbId);
        if (!photonView.isMine && PhotonNetwork.inRoom) return;
        VignetteModel.Settings settings = postProcessingProfile.vignette.settings;

        float hp = health;
        if (hp <= 0) hp = 1;
        settings.intensity = 0.35f + (0.3f * Mathf.Abs(1 - hp/maxHealth));
        //settings.intensity += 0.2f;
        postProcessingProfile.vignette.settings = settings;
    }    

    void InventorySelect()
    {
        //if (reloading) return;
        for (int i = 0; i < inventory.slots.Count; i++) {
            {
                if (Input.GetKeyDown((i+1).ToString()))
                {
                    InventorySlot slot = inventory.slots[i];
                    if (slot.item)
                    {
                        if (slot.item == heldItem)
                            Unequip(slot.item, true);
                        else
                            Equip(slot.item, true);
                    }
                    else
                    {
                        Equip(slot.item, true);
                    }
                }
            }
        }
    }


    void FlashLight()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (PhotonNetwork.inRoom)
                photonView.RPC("RPCFlashLight", PhotonTargets.AllBuffered);
            else
                RPCFlashLight();
        }
    }

    [PunRPC]
    public void RPCFlashLight()
    {
        flashLight.enabled = !flashLight.enabled;
    }

    void CameraZoom()
    {
        zooming = false;
        float targetView = 60;
        if (Input.GetMouseButton(1))
        {
            targetView = 40;
            zooming = true;
        }

        targetView -= (audioSource.volume * 5);

        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, targetView, 0.3f);
    }

    void ResetBools()
    {        
        freezeMovement.Update();
        freezeCamera.Update();
        animator.aiming.state = true;
    }

    RaycastHit hit;
    public RaycastHit IsLookingAt(float length, LayerMask layerMask)
    {                
        Physics.Raycast(camPivot.transform.position, camPivot.transform.forward, out hit, length, layerMask);
        return hit;
    }
    
    public override void UpdateItem()
    {
        if (heldItem)
        {
            heldItem.transform.localPosition = Vector3.zero;
            if (((PhotonNetwork.inRoom && photonView.isMine) || !PhotonNetwork.inRoom))
            {
                if (reloading)
                {
                    heldItem.transform.rotation = Quaternion.Lerp(heldItem.transform.rotation, Quaternion.LookRotation(-hand.transform.up + hand.transform.forward, Vector3.up), 0.4f);                    
                    //heldItem.transform.localEulerAngles = Vector3.Lerp(heldItem.transform.localEulerAngles, new Vector3(90, 0, 20), 0.4f);
                }
                else
                {                    
                    Quaternion rotation = hand.transform.rotation;
                    if (damageHit.collider)
                    {
                        Vector3 position = heldItem.transform.position;
                        if (heldItem is Gun)
                            if ((heldItem as Gun).projectilePosition)
                                position = (heldItem as Gun).projectilePosition.position;
                            else
                                position = heldItem.transform.position;
                        Vector3 direciton = damageHit.point - position;
                        if (direciton.sqrMagnitude > 1)
                            rotation = Quaternion.LookRotation(direciton);
                    }
                    else
                    {
                        rotation = Quaternion.LookRotation((camPivot.transform.position + (camPivot.transform.forward * 50)) - heldItem.transform.position);
                    }
                    //heldItem.transform.rotation = rotation;
                    heldItem.transform.rotation = Quaternion.Lerp(heldItem.transform.rotation, rotation, 0.5f);
                    //Quaternion offset = rotation * Quaternion.Inverse(hand.transform.rotation);                    
                    //heldItem.transform.localRotation = Quaternion.Lerp(heldItem.transform.localRotation, offset, 0.5f);                                        
                }
            }            
        }
    }

    void Interactable()
    {
        activeInteractable = null;
        //RaycastHit hit = IsLookingAt(5, interactLayerMask);                
        if (interacterHit.collider)
        {            
            Interactable interact = interacterHit.collider.GetComponent<Interactable>();
            if (interact)
                activeInteractable = interact;
        }


        if (heldItem)
        {
            if (interacterHit.collider == null)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (!reloading)
                    {
                        if (heldItem is Gun)
                            if ((heldItem as Gun).isEmpty && GetAmmo((heldItem as Gun).type).amount > 0)
                            {
                                Reload();
                            }
                    }
                    attacking = true;
                }
                if (!Input.GetMouseButton(0))
                    attacking = false;
                if (attacking && heldItem.CanUse() && !reloading)
                    heldItem.Use();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                if (heldItem is Gun)
                {
                    if (!(heldItem as Gun).isFull && GetAmmo((heldItem as Gun).type).amount > 0)
                    Reload();
                }
                    
            }

            if (Input.GetKeyDown(KeyCode.Q))
                Drop(heldItem);
        }

        if (interacterHit.collider)
        {
            if (activeInteractable)
            {
                if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.E))
                {                    
                    activeInteractable.Interact(this);
                }
            }
        }        
    }

    Vector3 movement;    

    void Movement()
    {
        if (freezeMovement) return;

        movement.x = Input.GetAxis("Horizontal");
        movement.z = Input.GetAxis("Vertical");

        movement = Vector3.ClampMagnitude(movement, 1);

        float speedMultiplier = 1 + Manager.playerSpeedBuff;
        if (Input.GetKey(KeyCode.LeftShift)) speedMultiplier *= 1.65f;

        if (ghost)
        {
            Vector3 direction = cam.transform.rotation * movement;
            direction.y = 0;
            if (movement.magnitude > 0)
                transform.position += direction * movementSpeed * speedMultiplier;            

            if (Input.GetKey(KeyCode.Space))
            {
                transform.position += Vector3.up * movementSpeed * speedMultiplier;
            }
            if (Input.GetKey(KeyCode.LeftControl))
            {
                transform.position += Vector3.down * movementSpeed * speedMultiplier;
            }
            return;
        }

        if (movement.magnitude > 0)
            Move(transform.rotation * movement, movementSpeed * speedMultiplier, false, false, true);
        else
            Move(Vector3.zero, 0, false, false);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump(Vector3.up * 5);
        }
    }

    Vector3 camRotation;
    Vector3 cameraShake;
    void CameraRotation()
    {        
        float mouseSensitivity = GlobalSettings.mouseSensitivity;
        if (zooming) mouseSensitivity *= 0.8f;

        if (!freezeCamera)
        {
            cameraEuler.y += Input.GetAxisRaw("Mouse X") * mouseSensitivity;
            cameraEuler.x -= Input.GetAxisRaw("Mouse Y") * mouseSensitivity;
            if (cameraEuler.x > 70) cameraEuler.x = 70;
            if (cameraEuler.x < -70) cameraEuler.x = -70;
        }

        camRotation.y = cameraEuler.y;

        rigidbody.rotation = Quaternion.Euler(camRotation);
        cameraShake.x = Random.Range(-1, 1);
        cameraShake.y = Random.Range(-1, 1);
        cameraShake.z = Random.Range(-1, 1);
        cameraShake *= audioSource.volume * 0.1f;        
        camPivot.transform.rotation = Quaternion.Euler(cameraEuler + cameraShake);           
    }
}

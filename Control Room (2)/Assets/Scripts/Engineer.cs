﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engineer : Soldier {

    public PowerGenerator activeGenerator;
    public TransmissionScreen activeTransmission;

    void Update()
    {
        if (dead) return;
        UpdateCharacter();

        //Follow();
        Movement();
        Detection();
        DetectRepair();
        ActiveTarget();
        RepairGenerator();
        RepairTransmission();
    }

    public void DetectRepair()
    {

        //if (detectionDelay % 5 != 0) return;
        //if (!detector.updated) return;
        if (activeTarget) return;
        if (activeTransmission || activeGenerator) return;
        if (activePath != null && activePath.nodes.Count > 0) return;

        PowerGenerator nearestGen = null;
        float genDistance = 1000;

        TransmissionScreen nearestTrans = null;
        float transDistance = 1000;

        foreach (GameObject detect in detector.detections)
        {
            if (detect == null) continue;
            PowerGenerator powerGen = detect.GetComponent<PowerGenerator>();
            TransmissionScreen trans = detect.GetComponent<TransmissionScreen>();
            if (powerGen && powerGen.disabled && detector.CanSee(transform.position + Vector3.up, powerGen.transform.position + Vector3.up, powerGen.gameObject, sightLayerMask))
            {             
                float distance = (transform.position - powerGen.transform.position).magnitude;
                if (distance < genDistance)
                {
                    genDistance = distance;
                    nearestGen = powerGen;
                }
            }
            
            else if (trans && (!trans.transmitting && !trans.diagnose && !trans.repair) && detector.CanSee(transform.position + Vector3.up, trans.standPosition - (trans.transform.forward * 0.1f), trans.gameObject, sightLayerMask))
            {                
                float distance = (transform.position - trans.transform.position).magnitude;
                if (distance < transDistance)
                {
                    transDistance = distance;
                    nearestTrans = trans;
                }                
            }

            if (nearestGen)
            {
                activeGenerator = nearestGen;
                activePath = Pathfind.GetPath(transform.position, activeGenerator.transform.position);
            }
            else if (nearestTrans)
            {
                activeTransmission = nearestTrans;
                activePath = Pathfind.GetPath(transform.position, activeTransmission.standPosition);
            }
        }
    }

    public override void SetPath(PFPath path)
    {
        base.SetPath(path);
        //MilitaryManager.RemoveAssignment(this);
        activeGenerator = null;
        activeTransmission = null;
    }

    public void RepairGenerator()
    {
        if (activeTarget) activeGenerator = null;
        if (!activeGenerator) return;
        if (!activeGenerator.disabled)
        {
            activeGenerator = null;
            return;
        }

        if ((transform.position - activeGenerator.transform.position).magnitude > 1.6f)
        {
            float speed = DistancedMovementSpeed(transform.position, activeGenerator.transform.position, movementSpeed, 0.5f);
            Move(activeGenerator.transform.position - transform.position, speed, true, true);
        }
        else
        {
            activePath = null;
            activeGenerator.Repair();            
        }
    }

    public void RepairTransmission()
    {
        if (activeTarget) activeTransmission = null;
        if (!activeTransmission) return;        

        if (activeTransmission.transmitting || activeTransmission.diagnose || activeTransmission.repair)
        {
            activeTransmission = null;
            return;
        }   

        if ((transform.position - activeTransmission.standPosition).magnitude > 1.6f)
        {
            float speed = DistancedMovementSpeed(transform.position, activeTransmission.standPosition, movementSpeed, 0.5f);
            Move(activeTransmission.standPosition - transform.position, speed, true, true);
        }
        else
        {
            if (!activeTransmission.activeMenu || !activeTransmission.activeMenu.gameObject.name.Contains("Transmission"))
            {
                foreach (WorldButton button in activeTransmission.GetComponentsInChildren<WorldButton>())
                {
                    if (button.gameObject.name.Contains("Transmission"))
                        button.Interact(this);
                }
            }

            if (!activeTransmission.diagnoseComplete)
            {
                activeTransmission.StartDiagnosis();
            }

            else
            {
                activeTransmission.RepairTransmission();
            }

            activePath = null;            
        }
    }
}

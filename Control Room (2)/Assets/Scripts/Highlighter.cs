﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlighter : MonoBehaviour {

    public List<WorldButton> buttons = new List<WorldButton>();
    public float multiplier;    

	void Start () {
		foreach(WorldButton button in GetComponentsInChildren<WorldButton>())
        {
            buttons.Add(button);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Player.isDead) return;
        RaycastHit hit = Player.interacterHit;
        foreach (WorldButton button in buttons)
        {
            if (hit.collider && hit.collider.gameObject == button.gameObject)
            {
                button.Highlight(multiplier);
            }
        }
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Extractor : Gun {

    public Sprite filledImage;    
    public LayerMask interactMask;
    public bool filled;
    public GameObject injector;

    public override void Initialize()
    {
        base.Initialize();
        ammo = 0;
    }

    public override void OnPickUp()
    {
        UpdateImage();
    }

    public void UpdateImage()
    {
        if (holder == Player.Instance)
        {
            Sprite activeImage = image;
            if (filled) activeImage = filledImage;
            holder.inventory.hud.UpdateImage(this, activeImage);
        }
    }

    [PunRPC]
    public override void RPCUse() {

        if (!CanUse()) return;
        SetAttackTime();
        if (Player.Instance != holder) return;

        RaycastHit hit = Player.Instance.IsLookingAt(5, interactMask);
            Collider col = hit.collider;
        
        if (!col)
        {
            Manager.PlayAudio(audioSource, fireClip);
            return;
        }
        
        if (!filled) {            
            Limb limb = col.gameObject.GetComponent<Limb>();
            if (limb && limb.characterType == CharacterType.Infected)
            {
                Manager.CreateParticle(ParticleType.Flesh, hit.point, Quaternion.LookRotation(-transform.forward));
                //Instantiate(fleshParticle, hit.point, Quaternion.LookRotation(-transform.forward));
                Fill(true, null);
            } else {
                Manager.PlayAudio(audioSource, fireClip);                
            }

        } else {
            CureComputer comp = col.gameObject.GetComponentInParent<CureComputer>();
            if (comp && col.gameObject == comp.platter)
            {
                Fill(false, comp);
            } else {
                Manager.PlayAudio(audioSource, fireClip);                
            }
        }
    }
    
    public void Fill(bool state, CureComputer computer) {
        if (state) {
            ammo = 1;
            filled = true;
            injector.transform.position -= transform.forward * 0.05f;
            Manager.PlayAudio(audioSource, hitFleshClip);
        } else {
            ammo = 0;
            filled = false;
            computer.GenerateCure();
            injector.transform.position += transform.forward * 0.05f;
            Manager.PlayAudio(audioSource, hitMetalClip);
        }
        UpdateImage();
    }
}

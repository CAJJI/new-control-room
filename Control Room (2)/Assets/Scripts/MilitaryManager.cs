﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitCode
{
    A, B, C, D
}


[System.Serializable]
public class MilitaryUnit
{
    public Color color;
    public List<Soldier> soldiers = new List<Soldier>();
    public UnitCode unitCode;
    
    public PFPath activePath;

    public Vector3 GetCurrentPosition()
    {
        Vector3 position = Vector3.zero;
        for (int i = 0; i < soldiers.Count; i++) {
            if (soldiers[i] == null) soldiers.Remove(soldiers[i]);
            else
            {
                position += soldiers[i].transform.position;
            }
        }
        position /= soldiers.Count;
        return position;
    }

    public void AssignPathFormation(PFPath path)
    {
        if (path == null || path.nodes.Count == 0) return;
        PFPath[] paths = new PFPath[soldiers.Count];

        foreach (PF_Node node in path.nodes)
        {
            List<PF_Node> ignore = new List<PF_Node>();
            for (int i = 0; i < paths.Length; i++)
            {                
                PF_Node formationNode = Pathfind.GetNearestAvailable(node, node.transform.position, ignore, false);
                ignore.Add(formationNode);
                if (paths[i] == null)
                {
                    paths[i] = new PFPath();                    
                }
                if (paths[i].nodes.Count > 0)
                {
                    PFPath addPath = Pathfind.GetPath(paths[i].nodes[paths[i].nodes.Count-1], formationNode);
                    paths[i].nodes.AddRange(addPath.nodes);
                }
                else
                    paths[i].nodes.Add(formationNode);                
            }
        }
        
        for (int i = 0; i < paths.Length; i++)
        {
            if (soldiers[i] != null)
            {
                PFPath connectPath = Pathfind.GetPath(soldiers[i].transform.position, paths[i].nodes[0].transform.position);
                connectPath.nodes.AddRange(paths[i].nodes);

                soldiers[i].SetPath(connectPath);
            }
        }        
    }
}

public class AssignUnit
{

    public Soldier soldier;
    public GameObject gameObject;

    public AssignUnit(Soldier soldier, GameObject gameObject)
    {
        this.soldier = soldier;
        this.gameObject = gameObject;
    }
}

public class MilitaryManager : Singleton<MilitaryManager> {

    public List<MilitaryUnit> units;
    public List<AssignUnit> assignedUnits = new List<AssignUnit>();

    public static MilitaryUnit GetUnit(UnitCode unitCode)
    {
        foreach(MilitaryUnit unit in Instance.units)
        {
            if (unit.unitCode == unitCode)
                return unit;
        }
        return null;
    }

    public static void RegisterUnit(Soldier soldier)
    {
        MilitaryUnit regUnit = soldier.militaryUnit;
        GetUnit(regUnit.unitCode).soldiers.Add(soldier);
    }

    public static void RemoveAssignment(Soldier soldier)
    {
        for (int i = 0; i < Instance.assignedUnits.Count; i++)
        {
            if (soldier == Instance.assignedUnits[i].soldier)
                Instance.assignedUnits.Remove(Instance.assignedUnits[i]);
        }
    }

    public static bool Assign(Soldier soldier, GameObject gameObject)
    {
        for (int i = 0; i < Instance.assignedUnits.Count; i++)
        {
            if (Instance.assignedUnits[i].gameObject == gameObject)
                return false;
        }
        Instance.assignedUnits.Add(new AssignUnit(soldier, gameObject));
        return true;
    }
}

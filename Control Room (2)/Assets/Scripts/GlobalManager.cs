﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Analytics;


[System.Serializable]
public class LevelCompletion
{
    public Level name;
    public Difficulty difficulty;
    public bool withCure;

    public LevelCompletion(Level level, Difficulty difficulty, bool withCure)
    {
        name = level;
        this.difficulty = difficulty;
        this.withCure = withCure;
    }
}


public class GlobalManager : Singleton<GlobalManager> {

    public float gameVersion;

    public static bool chatting;
    public static bool connected;
    public LevelCompletion[] levelCompletions;
    public static LevelInfo levelInfo = new LevelInfo();
    public GameObject blackScreenPrefab;
    Image blackScreen;
    public int loadingScreenIndex;
    public int loadSceneIndex;
    public bool loadScene;
    public static bool loading;

    public bool loadImmediate;
    public bool continueToScene;

    public static bool survived;
    public static bool levelCompleted;
    public static bool hasCure;
    public static bool cureGenerated;

    public static int playersSurvived;
    public static int totalSoldiers;
    public static int totalInfected;
    public static int deadSoldiers;
    public static int deadInfected;
    public static int totalTransmissionRepairs;
    public static float totalTime;

    public static int playerID;

    public static Room currentRoom;

    public GameDef gameDef;
    static GameDef _gameDef;
    public static GameDef GameDef { get { if (Instance == null) { if (_gameDef == null) _gameDef = Resources.Load<GameDef>("GameDef"); return _gameDef; } return Instance.gameDef; } }

    private void Start()
    {
        Pathfind.areas = new List<PF_Area>();

        PhotonNetwork.sendRate = 30;
        PhotonNetwork.sendRateOnSerialize = 30;
        PhotonNetwork.autoJoinLobby = true;
        if (!PlayerPrefs.HasKey("PlayerID"))
            PlayerPrefs.SetInt("PlayerID", Random.Range(10000, 99999));
        playerID = PlayerPrefs.GetInt("PlayerID");
#if UNITY_EDITOR
        playerID = 10000;
#endif

        Analytics.CustomEvent("PlayerOnMainMenu", new Dictionary<string, object> { { "playerID", playerID } });

        if (this != Instance)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;

        AssignMixer();
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        ResetScene();
        AssignMixer();

        continueToScene = false;
        if (!loadImmediate)        
            StartCoroutine(FadeOut());        
        if (loadScene)
        {
            StartCoroutine(LoadSceneAsync(loadSceneIndex));
        }
        loadImmediate = false;
    }    

    void AssignMixer()
    {
        foreach(AudioSource source in FindObjectsOfType<AudioSource>())
        {
            source.outputAudioMixerGroup = gameDef.GetMixer();
        }
    }


    void ResetScene()
    {
        Manager manager = Manager.Instance;
        //if (manager)
        {            
            //manager.Reset();
        }
        Time.timeScale = 1;
        if (PauseMenu.Instance)
        PauseMenu.Pause(false);
    }

    public static string FilterText(string text)
    {
        foreach (string word in GameDef.filteredWords)
        {            
            if (text.ToLower().Contains(word))
            {
                string censor = "";
                for (int i = 0; i < word.Length; i++)
                    censor += "*";
                text = text.ToLower().Replace(word, censor);                
            }
        }
        return text;
    }

    public static bool ContainsFilteredWord(string text)
    {
        foreach (string word in GameDef.filteredWords)
        {
            if (text.ToLower().Contains(word))
            {
                return true;                
            }            
        }
        return false;
    }

    public void LoadImmediate(int scene)
    {
        SceneManager.LoadScene(scene);
        loadImmediate = true;
    }

    public IEnumerator FadeOut()
    {        
        blackScreen = Instantiate(blackScreenPrefab, transform.position, Quaternion.identity).GetComponent<Image>();
        blackScreen.transform.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
        blackScreen.transform.position = blackScreen.transform.parent.position;
        Color color = blackScreen.color;
        color.a = 1;
        blackScreen.color = color;        
        yield return new WaitForSeconds(2);
        while (color.a > 0)
        {            
            color.a -= Time.deltaTime * 2;
            blackScreen.color = color;
            yield return null;
        }
        Destroy(blackScreen);
    }

    public void LoadScene(int scene)
    {
        if (loadScene) return;
        loadScene = true;
        loadSceneIndex = scene;
        blackScreen = Instantiate(blackScreenPrefab, transform.position, Quaternion.identity).GetComponent<Image>();
        blackScreen.transform.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
        blackScreen.transform.position = blackScreen.transform.parent.position;
        
        //StartCoroutine(LoadSceneAsync(scene));
        StartCoroutine(GoToLoadingScreen());
    }    

    IEnumerator GoToLoadingScreen()
    {
        Color color = blackScreen.color;
        while (color.a < 1)
        {
            color.a += Time.deltaTime * 2;
            blackScreen.color = color;
            yield return null;
        }        
        SceneManager.LoadScene(loadingScreenIndex);
    }

    public bool FadeToBlack()
    {
        return FadeToBlack(2);
    }

    public bool FadeToBlack(float speedMultiplier)
    {
        if (!blackScreen) CreateBlackScreen(0);
        Color color = blackScreen.color;
        if (color.a >= 1) return true;
        color.a += Time.fixedDeltaTime * speedMultiplier;
        blackScreen.color = color;
        return false;
    }

    public void CreateBlackScreen(int alpha)
    {
        blackScreen = Instantiate(blackScreenPrefab, transform.position, Quaternion.identity).GetComponent<Image>();
        blackScreen.transform.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
        blackScreen.transform.position = blackScreen.transform.parent.position;
        Color color = blackScreen.color;
        color.a = alpha;
        blackScreen.color = color;
    }

    IEnumerator LoadSceneAsync(int scene)
    {        
        loading = true;
        yield return new WaitForSeconds(1);        
        loadScene = false;
        AsyncOperation async = SceneManager.LoadSceneAsync(scene);
        async.allowSceneActivation = false;
        bool loadDelay = false;
        while (!async.isDone && !async.allowSceneActivation)
        {            
            if (async.progress >= 0.9f && blackScreen.color.a <= 0)
            {
                if (loadDelay)
                {
                    yield return new WaitForSeconds(2);
                    loadDelay = true;
                }
                loading = false;
                if (Input.GetKeyDown(KeyCode.Space)) continueToScene = true;
            }
            if (continueToScene)
            {
                if (!blackScreen)
                {
                    blackScreen = Instantiate(blackScreenPrefab, transform.position, Quaternion.identity).GetComponent<Image>();
                    blackScreen.transform.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
                    blackScreen.transform.position = blackScreen.transform.parent.position;
                }
                else
                {
                    Color color = blackScreen.color;
                    while (color.a < 1)
                    {
                        color.a += Time.deltaTime * 2;
                        blackScreen.color = color;
                        yield return null;
                    }
                    async.allowSceneActivation = true;
                }

            }
            yield return null;
        }
    }

    public static void RegisterCharacter(Character character)
    {
        if (character.GetComponent<Infected>())
            totalInfected++;

        if (character.GetComponent<Soldier>())
            totalSoldiers++;
    }

    public static void RegisterDeath(Character character)
    {
        if (character.GetComponent<Infected>())
        {
            deadInfected++;
        }

        if (character.GetComponent<Soldier>())
            deadSoldiers++;
    }

    public static void RegisterTransmissionRepair()
    {
        totalTransmissionRepairs++;
    }

    public void LevelComplete(Level level)
    {                
        PlayerPrefs.SetInt(System.Enum.GetName(typeof(Level), level), levelInfo.difficulty + 1);
        levelCompleted = true;
        survived = true;
        //Manager.Instance.Quit();
    }

    public void Quit()
    {
        Application.Quit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour {

    public Text loadingText;
    public Text tipText;
    public string[] tips;
    public int currentTip;
    float tipTimer;


    private void Start()
    {
        //currentTip = Random.Range(0, tips.Length - 1);
        StartCoroutine(LoadingText());     
    }

    private void Update()
    {
        //Tips();
        CycleTips();
    }

    void Tips()
    {
        if (Input.GetKeyDown(KeyCode.D))
            currentTip++;
        if (Input.GetKeyDown(KeyCode.A))
            currentTip--;        
    }

    void CycleTips()
    {
        if (tipTimer == 0)
        {
            Color color = tipText.color;
            color.a += Time.deltaTime;
            tipText.color = color;
            if (color.a >= 1)
            {                
                tipTimer = 1;
            }
        }
        else
        {
            tipTimer+= Time.deltaTime;
        }
        if (tipTimer > 4)
        {
            Color color = tipText.color;
            color.a -= Time.deltaTime;
            tipText.color = color;
            if (color.a <= 0)
            {
                currentTip++;
                tipTimer = 0;
            }
        }

        if (currentTip < 0) currentTip = tips.Length - 1;
        if (currentTip >= tips.Length) currentTip = 0;

        tipText.text = tips[currentTip];
    }
    
    IEnumerator LoadingText()
    {
        int count = 0;        
        while (GlobalManager.loading)
        {
            string loadingString = "Loading";
            for (int i = 0; i < count % 3; i++)
            {
                loadingString += ".";
            }
            loadingText.text = loadingString;
            count++;
            yield return new WaitForSeconds(1f);
        }
        loadingText.text = "Press 'Space' to continue.";
    }
}

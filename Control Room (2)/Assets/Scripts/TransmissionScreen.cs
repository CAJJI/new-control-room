﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TransmissionScreen : MultiplayerBehaviour {

    public bool disable;
    public bool transmitting;
    public bool diagnose;
    public bool diagnoseComplete;
    public bool repair;
    public Transform callPosition;
    public Vector3 standPosition;

    public GameObject activeMenu;
    public GameObject diagnoseButton;
    public GameObject repairButton;
    public TextMesh transmitStatusText;

    public GameObject UIPrefab;
    public GameObject UIAlert;
    AudioSource audioSource;
    public AudioClip clickClip;
    public AudioClip activeClip;
    public AudioClip transDiagClip;

    public GameObject securityMenu;
    public GameObject screen;
    public Color alertColor;
    public Color initialColor;

    public UnityEvent disableEvent;
    public UnityEvent repairEvent;

    private void Start()
    {
        DetectionManager.AddDetectable(gameObject);
        if (disable) return;
        standPosition = transform.GetChild(0).position;
        audioSource = GetComponent<AudioSource>();
        UIAlert = Instantiate(UIPrefab, transform.position + (transform.forward*2.5f) + (Vector3.up * 45), Quaternion.identity);
        UIAlert.SetActive(false);

        initialColor = screen.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        for (int i = 0; i < securityMenu.transform.childCount; i++) {
            securityMenu.transform.GetChild(i).GetComponentInChildren<TextMesh>().color = MilitaryManager.GetUnit((UnitCode)i).color;
        }

        if (!transmitting)
            StopTransmission();
    }

    private void Update()
    {
        HighlightButtons();
    }

    void HighlightButtons()
    {
        foreach (WorldButton button in GetComponentsInChildren<WorldButton>())
        {
            if (Player.isDead) return;
            RaycastHit hit = Player.interacterHit;
            if (hit.collider && hit.collider.gameObject == button.gameObject)
            {
                button.Highlight(2);
            }
        }
    }

    public void RepairTransmission()
    {
        StartCoroutine(Repairing());
        Manager.PlayAudio(audioSource, clickClip);
    }    

    public void StartDiagnosis()
    {
        Manager.PlayAudio(audioSource, clickClip);
        StartCoroutine(Diagnose());        
    }

    public void StopTransmission()
    {
        if (PhotonNetwork.inRoom) {
            photonView.RPC("RPCStopTransmission", PhotonTargets.AllBuffered);
        } else {
            RPCStopTransmission();
        }
    }    

    [PunRPC]
    public void RPCStopTransmission() {        
        transmitting = false;
        Manager.CreateAudio(transform.position, Manager.Instance.transDownClip, Vector3.zero, 0.2f);
        CRInterface.Instance.transmissionDown = true;
        ObjectiveUI.UpdateText();
        diagnoseButton.SetActive(true);
        transmitStatusText.text = "NO SIGNAL FOUND.";
        StartCoroutine(FlashAlert());
        disableEvent.Invoke();
    }

    IEnumerator Repairing()
    {
        diagnoseComplete = false;
        repair = true;
        screen.GetComponent<Renderer>().material.SetColor("_EmissionColor", initialColor);
        repairButton.SetActive(false);
        transmitStatusText.text = "Repairing...";
        yield return new WaitForSeconds(1);
        repair = false;
        transmitting = true;
        CRInterface.Instance.transmissionDown = false;
        transmitStatusText.text = "Repair complete.";
        Manager.PlayAudio(audioSource, activeClip);
        yield return new WaitForSeconds(1);
        transmitStatusText.text = "TRANSMITTING.";
        GlobalManager.RegisterTransmissionRepair();
        repairEvent.Invoke();
        ObjectiveUI.UpdateText();
    }

    IEnumerator Diagnose()
    {
        diagnoseComplete = false;
        diagnose = true;
        diagnoseButton.SetActive(false);
        transmitStatusText.text = "Please wait...";

        yield return new WaitForSeconds(1);
        Manager.PlayAudio(audioSource, transDiagClip);

        yield return new WaitForSeconds(4);
        diagnoseComplete = true;
        diagnose = false;
        repairButton.SetActive(true);
        Manager.PlayAudio(audioSource, activeClip);
        transmitStatusText.text = "Diagnosis Complete.";
    }

    IEnumerator FlashAlert()
    {
        while (!transmitting)
        {
            CRInterface.Instance.LogText("REPAIR TRANSMISSION");
            screen.GetComponent<Renderer>().material.SetColor("_EmissionColor", alertColor);
            UIAlert.SetActive(true);
            yield return new WaitForSeconds(1);            
            CRInterface.Instance.LogText("LOCATION MARKED");
            screen.GetComponent<Renderer>().material.SetColor("_EmissionColor", initialColor);
            UIAlert.SetActive(false);
            yield return new WaitForSeconds(1);
        }
    }

    public void CallUnit(int unit)
    {
        if (PhotonNetwork.inRoom) {
            if (unit > -1)
                CRInterface.Instance.SetMarker(unit, transform.position);
        } else
            CRInterface.Instance.SetUnitDestination(MilitaryManager.GetUnit((UnitCode)unit), callPosition.position);
        OpenMenu(null);
    }

    public void CloseMenu()
    {
        if (activeMenu)
            activeMenu.SetActive(false);
        activeMenu = null;
    }

    public void OpenMenu(GameObject menu)
    {
        Manager.PlayAudio(audioSource, clickClip);
        if (activeMenu)
        {
            activeMenu.SetActive(false);
            if (activeMenu == menu)
            {
                activeMenu = null;
                return;
            }
        }
        activeMenu = menu;
        if (activeMenu)
        activeMenu.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class GlobalSettings : Singleton<GlobalSettings> {

    public enum QualityLevel
    {
        High, Optimized
    }

    [Range(0.1f, 50)]
    public static float mouseSensitivity = 2;
    [Range(1,100)]
    public static int ragdollLimit = 10;

    public static QualityLevel characterQuality = QualityLevel.High;
    public static QualityLevel lightQuality = QualityLevel.High;
    public static QualityLevel postProcessingQuality = QualityLevel.High;

    public static bool motionBlur;
    
    public static float masterVolume { set { GlobalManager.GameDef.audioMixer.SetFloat("masterVolume", GetVolume(value)); } get {return GetVolume(); } }

    private void Start()
    {            
        mouseSensitivity = GetKey("mouseSensitivity", mouseSensitivity);
        ragdollLimit = (int)GetKey("ragdollLimit", ragdollLimit);

        characterQuality = (QualityLevel)GetKey("characterQuality", (int)characterQuality);
        lightQuality = (QualityLevel)GetKey("lightQuality", (int)lightQuality);
        postProcessingQuality = (QualityLevel)GetKey("postProcessingQuality", (int)postProcessingQuality);        

        motionBlur = System.Convert.ToBoolean(GetKey("motionBlur", System.Convert.ToInt32(motionBlur)));

        masterVolume = GetKey("masterVolume", masterVolume);

        ApplySettings();
    }

    public static float GetKey(string key, float value)
    {
        if (PlayerPrefs.HasKey(key))
        {            
            return PlayerPrefs.GetFloat(key);
        }
        else
        {
            return SetKey(key, value);
        }
    }

    public static float SetKey(string key, float value)
    {
        PlayerPrefs.SetFloat(key, value);
        PlayerPrefs.Save();        
        return value;
    }

    public static float GetVolume(float percentage)
    {
        float volume = -80;
        percentage = (percentage / 100) * 80;
        return volume + percentage;
    }

    public static float GetVolume()
    {
        float value = 0;
        GlobalManager.GameDef.audioMixer.GetFloat("masterVolume", out value);        
        return ((value + 80) / 80) * 100;
    }

    public static void ApplySettings()
    {
        PostProcessingProfile profile = GlobalManager.GameDef.postProcessingProfile;

        profile.motionBlur.enabled = motionBlur;

        bool state = (postProcessingQuality == QualityLevel.High);
        profile.bloom.enabled = state;
        profile.antialiasing.enabled = state;
        profile.ambientOcclusion.enabled = state;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.Analytics;

public class Manager : Singleton<Manager> {

    public static bool debugMode;

    public GameObject playerPrefab;
    public Transform playerSpawnPoint;

    public bool rescueAudioCreated;

    public Level level;
    public bool disable;
    public bool disableLockDown;
    public float surviveForMinutes = 5;
    public static float playerSpeedBuff = 0;

    public GameObject lights;
    public GameObject lightsLowRes;

    public GameObject soldierPrefab;
    public GameObject engineerPrefab;    
    public List<Transform> soldierSpawnPoints;
    public GameObject audioSourcePrefab;    

    public List<MultiplayerBehaviour> multiplayerBehaviours = new List<MultiplayerBehaviour>();

    public static Vector3 timeVector { get{ return new Vector3(Mathf.Floor(timeRemaining / 60), Mathf.RoundToInt(timeRemaining % 60), Mathf.RoundToInt((timeRemaining * 1000) % 1000)); } }    
    public static float timeRemaining;
    public float delayLockDown;
    public static bool startLockDown;
    public static bool lockDown;
    public float[] transmissionDownTimes;
    public int transmissionDownIndex;
    public TransmissionScreen[] transmissionScreens;
    public AudioSource audioSource;
    public AudioClip lockDownClip;
    public AudioClip lightSwitchClip;
    public AudioClip ambienceClip;
    public AudioClip transDownClip;
    public AudioClip rescueClip;
    
    public List<GameObject> ragdolls = new List<GameObject>();

    public List<ParticleEmitter> particles = new List<ParticleEmitter>();
    public List<AudioPlayer> audioPlayers = new List<AudioPlayer>();

    static Transform _UIContainer;
    public static Transform UIContainer { get { if (!_UIContainer) _UIContainer = new GameObject("UIContainer").transform; return _UIContainer; } }


    private void Awake()
    {        
        if (!PhotonNetwork.inRoom && playerSpawnPoint)
        {
            Player player = Instantiate(playerPrefab, playerSpawnPoint.position, playerSpawnPoint.rotation).GetComponent<Player>();            
        }
        if (disable) return;
        Reset();
        SetDifficulty();
        SpawnSoldiers();
        SetLights();        
    }

    public void RegisterID(MultiplayerBehaviour behaviour)
    {
        if (PhotonNetwork.inRoom && behaviour.photonView != null)
            behaviour.id = behaviour.photonView.viewID;
        else
            behaviour.id = multiplayerBehaviours.Count;
        multiplayerBehaviours.Add(behaviour);

        
    }

    void InitializeParticles()
    {
        GameObject parent = new GameObject("Particles");
        for (int i = 0; i < 50; i++)
        {
            ParticleEmitter particle = Instantiate(GlobalManager.GameDef.particlePrefab, Vector3.zero, Quaternion.identity, parent.transform).GetComponent<ParticleEmitter>();            
            particles.Add(particle);
        }
    }

    public void SetLights()
    {
        bool optimize = GlobalSettings.lightQuality == GlobalSettings.QualityLevel.Optimized;
        if (lights)
        lights.SetActive(!optimize);
        if (lightsLowRes)
        lightsLowRes.SetActive(optimize);        
    }

    void SetDifficulty()
    {
        if (GlobalManager.levelInfo.difficulty > 0)
        {
            //Player.Instance.health /= GlobalManager.levelInfo.difficulty;
            //Player.Instance.maxHealth /= GlobalManager.levelInfo.difficulty;
        }
    }

    public virtual void RemovePlayer(Player player, bool survived, bool hasCure) {

    }

    public T GetBehaviour<T>(int ID) where T : MultiplayerBehaviour
    {
        //if (PhotonNetwork.inRoom)
        //return PhotonView.Find(ID).GetComponent<T>();

        for (int i = 0; i < multiplayerBehaviours.Count; i++) 
        {
            if (multiplayerBehaviours[i] == null)
            {
                multiplayerBehaviours.Remove(multiplayerBehaviours[i]);
                i--;
                continue;
            }
            //if (PhotonNetwork.inRoom && !multiplayerBehaviours[i].photonView) { Debug.Log(multiplayerBehaviours[i].name + " is missing a photonView"); continue; }
            if (PhotonNetwork.inRoom && !multiplayerBehaviours[i].photonView) { continue; }
            if (multiplayerBehaviours[i].id == ID)
                return multiplayerBehaviours[i] as T;
        }
        return null;
    }

    void SpawnSoldiers()
    {
        if (GlobalManager.levelInfo == null || GlobalManager.levelInfo.soldierInfos.Length < 4 || GlobalManager.levelInfo.soldierInfos[0] == null) return;                

        for (int i = 0; i < 4; i++)
        {
            Transform spawnPoint = soldierSpawnPoints[i];
            int currentSoldier = 0;
            foreach(SoldierInfo.SoldierClass soldierClass in GlobalManager.levelInfo.soldierInfos[i].soldierClasses)
            {
                int length = GlobalManager.levelInfo.soldierInfos[i].soldierClasses.Count;
                Vector3 position = Quaternion.Euler(0,(360/length)*currentSoldier,0) * spawnPoint.forward + spawnPoint.position;
                Quaternion rotation = Quaternion.LookRotation(spawnPoint.position - position);
                GameObject prefab = soldierPrefab;
                if (soldierClass == SoldierInfo.SoldierClass.Engineer)
                    prefab = engineerPrefab;

                Soldier soldier = Instantiate(prefab, position, rotation).GetComponent<Soldier>();
                soldier.militaryUnit.unitCode = (UnitCode)i;
                currentSoldier++;
            }            
        }
    }

    private void Start()
    {
        if (disable) return;
        PostProcessingProfile postProcessingProfile = GlobalManager.GameDef.postProcessingProfile;

        VignetteModel.Settings settings = postProcessingProfile.vignette.settings;
        settings.intensity = 0.35f;
        postProcessingProfile.vignette.settings = settings;

        UnityEngine.Cursor.lockState = CursorLockMode.Locked;
        UnityEngine.Cursor.visible = false;

        transmissionScreens = GameObject.FindObjectsOfType<TransmissionScreen>();
        InitializeParticles();
        InitializeAudio();
    }
    private void OnApplicationQuit()
    {
        if (disable) return;
        PostProcessingProfile postProcessingProfile = GlobalManager.GameDef.postProcessingProfile;

        VignetteModel.Settings settings = postProcessingProfile.vignette.settings;
        settings.intensity = 0.35f;
        postProcessingProfile.vignette.settings = settings;        


    }

    public virtual void Reset()
    {
        StopAllCoroutines();

        GlobalManager.playersSurvived = 0;
        GlobalManager.totalSoldiers = 0;
        GlobalManager.totalInfected = 0;
        GlobalManager.deadSoldiers = 0;
        GlobalManager.deadInfected = 0;
        GlobalManager.totalTransmissionRepairs = 0;
        GlobalManager.totalTime = 0;
        GlobalManager.chatting = false;

        foreach(Soldier soldier in GameObject.FindObjectsOfType<Soldier>())
        {
            GlobalManager.totalSoldiers++;
        }

        startLockDown = false;
        lockDown = false;
        timeRemaining = 0;
    }    

    private void Update()
    {
        DebugMode();

            if (disable) return;
        GlobalManager.totalTime += Time.deltaTime;
        Pausing();
        DetectionManager.Update();

        if (disableLockDown) return;
        LockDown();
        Transmissions();        
    }

    public void DebugMode()
    {
        if (Application.isEditor)
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                debugMode = !debugMode;
                Debug.Log("Set debugMode " + debugMode);
            }
        }
    }
    
    public void Pausing()
    {
        if (!PauseMenu.paused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                UnityEngine.Cursor.lockState = CursorLockMode.Locked;
                UnityEngine.Cursor.visible = false;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PauseMenu.Pause(true);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PauseMenu.Pause(false);
            }
        }
    }

    public virtual void Transmissions()
    {
        if (!lockDown) return;
        if (transmissionDownIndex > transmissionDownTimes.Length-1) return;
        if (timeRemaining < transmissionDownTimes[transmissionDownIndex] * 60)
        {
            transmissionDownIndex++;            
            transmissionScreens[Random.Range(0, transmissionScreens.Length)].StopTransmission();
        }
    }    

    public virtual void LockDown()
    {
        if (!startLockDown)
        {
            if (Time.timeSinceLevelLoad > delayLockDown)
            {
                StartCoroutine(StartLockDown());                
            }            
        }        
    }    

    void InitializeAudio()
    {
        GameObject parent = new GameObject("AudioPlayers");
        for (int i = 0; i < 50; i++)
        {            
            AudioPlayer audio = Instantiate(Instance.audioSourcePrefab, Vector3.zero, Quaternion.identity, parent.transform).GetComponent<AudioPlayer>();            
            audio.Play(GlobalManager.GameDef.testClip, Vector2.one, 1, 20);
            audio.audioSource.volume = 0;
            //audio.gameObject.SetActive(false);
            audioPlayers.Add(audio);
        }
    }

    public static AudioSource CreateAudio(Vector3 position, AudioClip audioClip, Vector2 pitchRange, float spatialBlend, float maxDistance = 20)
    {
        //AudioSource audioSource = Instantiate(Instance.audioSourcePrefab, position, Quaternion.identity).GetComponent<AudioSource>();
        AudioPlayer audioPlayer = null;
        foreach(AudioPlayer audio in Instance.audioPlayers)
        {
            if (!audio.gameObject.activeSelf)
            {
                audioPlayer = audio;
                break;
            }
        }                
        if (!audioPlayer) return null;
        audioPlayer.transform.position = position;        
        audioPlayer.Play(audioClip, pitchRange, spatialBlend, maxDistance);        
        return audioPlayer.audioSource;
    }

    public static AudioSource CreateAudio(Vector3 position, AudioClip audioClip, Vector2 pitchRange)
    {
        return CreateAudio(position, audioClip, pitchRange, 0);
    }

    public static AudioSource CreateAudio(Vector3 position, AudioClip audioClip)
    {
        return CreateAudio(position, audioClip, Vector2.zero);
    }

    public static void PlayAudio(AudioSource audioSource, AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.Play();
    }

    public static ParticleEmitter CreateParticle(ParticleType type, Vector3 position, Quaternion rotation){

        foreach(ParticleEmitter particle in Instance.particles)
        {
            if (particle.Play(type))
            {
                particle.transform.position = position;
                particle.transform.rotation = rotation;
                return particle;
            }
        }
        return null;
    }

    public virtual IEnumerator StartLockDown()
    {
        startLockDown = true;        
        timeRemaining = 60 * surviveForMinutes;        
        foreach (BlastDoor door in GameObject.FindObjectsOfType<BlastDoor>())
        {
            door.OpenDoor(false);
        }
        yield return new WaitForSeconds(3);

        CRInterface.Instance.LogText("LOCK DOWN");
        float timer = 0;
        int state = -1;
        GameObject audioSourceGO = CreateAudio(Player.Instance.transform.position, lockDownClip, Vector3.one * 0.9f, 0.1f).gameObject;
        AudioSource newAudioSource = audioSourceGO.GetComponent<AudioSource>();
        while (audioSourceGO != null && audioSourceGO.activeSelf)
        {
            if (PauseMenu.paused)
            {
                newAudioSource.Pause();
            }
            else if (!newAudioSource.isPlaying)
            {
                newAudioSource.UnPause();
            }
            timer+= Time.deltaTime * 70;
            if (timer > 100)
            {
                state++;
                timer = 0;
                if (state == 0)
                    CRInterface.Instance.LogText("LOCK DOWN");
                else if (state == 1)
                    CRInterface.Instance.LogText("RESCUE ALERTED");                
                else if (state == 2)
                {
                    CRInterface.Instance.LogText("MAINTAIN TRANSMISSION");
                    state = -1;
                }
            }
            yield return null;
        }
        CreateAudio(transform.position, lightSwitchClip, Vector3.zero, 0.5f);
        PlayAudio(audioSource, ambienceClip);

        lockDown = true;

        bool skip = false;
        while (timeRemaining > 0 && !skip)
        {
            //if (Input.GetKeyDown(KeyCode.E))
                //skip = true;
                
            if (!rescueAudioCreated && timeRemaining < 5)
            {
                GameObject rescueAudioGO = CreateAudio(transform.position, rescueClip, Vector2.zero, 0.1f).gameObject;
                rescueAudioCreated = true;
            }
            if (!CRInterface.Instance.transmissionDown)
            timeRemaining -= Time.deltaTime;            
            yield return null;
        }
        
        if (Player.isDead)
        {
            yield return null;
        }

        Time.timeScale = 0;
        Debug.Log("Complete");
                
        while (!GlobalManager.Instance.FadeToBlack(1))
        {
            yield return null;
        }

        //GlobalManager.Instance.LevelComplete(level);
        Quit(true, true);
    }    

    public virtual IEnumerator PlayerDeath(int id)
    {
        DimAudio();
        yield return new WaitForSeconds(3);
        while (!Input.GetMouseButtonDown(0))
        {            
            yield return null;
        }        

        GameObject loadingScreenPrefab = Resources.Load<GameObject>("LoadingScreen");
        Instantiate(loadingScreenPrefab, FindObjectOfType<Canvas>().transform);
        FindObjectOfType<AudioListener>().transform.position = Vector3.down * 1000f;
        yield return null;
        
        Quit(true, false);
    }

    public void DimAudio()
    {
        AudioListener audioListener = new GameObject().AddComponent<AudioListener>();
        audioListener.transform.position = Camera.main.transform.position + Vector3.down * 10;
        Camera.main.gameObject.GetComponent<AudioListener>().enabled = false;
    }

    public void Pause(bool state)
    {
        PauseMenu.Pause(state);
    }

    public virtual void Quit(bool showResults, bool survived)
    {
        GlobalManager.survived = survived;
        GlobalManager.levelCompleted = showResults;

        Debug.Log(GlobalManager.levelCompleted);

        if (survived && showResults)
        {
            GlobalManager.Instance.LevelComplete(GlobalManager.levelInfo.level);            
        }

        Debug.Log(GlobalManager.levelCompleted);
        
        if (!showResults)
        {
            Analytics.CustomEvent("QuitToMenu", new Dictionary<string, object> { { "playerID", GlobalManager.playerID } });

            if (PhotonNetwork.inRoom)
            {
                PhotonNetwork.LeaveRoom();                
            }
        }        

        Pathfind.areas = new List<PF_Area>();
        GlobalManager.Instance.LoadImmediate(0);
    }

    public virtual void Quit()
    {
        Quit(false, false);
    }

    public static void AddRagdoll(GameObject GO)
    {
        Instance.ragdolls.Add(GO);
        if (Instance.ragdolls.Count > GlobalSettings.ragdollLimit)
        {
            Destroy(Instance.ragdolls[0]);
            Instance.ragdolls.Remove(Instance.ragdolls[0]);
        }
    }

    public void SetKinematic(GameObject GO)
    {
        StartCoroutine("IESetKinematic", GO);
    }

    IEnumerator IESetKinematic(GameObject GO)
    {
        for(int i = 0; i < 1; i++)
        yield return null;        
        GO.GetComponent<Rigidbody>().isKinematic = true;       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : Character {

    public MilitaryUnit militaryUnit;
    public PFPath repositionPath;
    public BlastDoor waitingDoor;
    //public Character follow;
    //public float followDelay;
    int setFollowDelay = 20;
    public Vector3 activePosition;
    
    void Start()
    {
        activePosition = transform.position;
        //setDetectionDelay = 20;
        //detectionDelay = Random.Range(0, setDetectionDelay);
        //detectionDelay = (4 * (int)militaryUnit.unitCode) + (1 + MilitaryManager.GetUnit(militaryUnit.unitCode).soldiers.Count);
        DetectionManager.AddSoldier(detector);
        //followDelay = detectionDelay;
        maxHealth = GlobalManager.GameDef.GetCharacterDef(CharacterType.Soldier).health;
        health = maxHealth;
        mountedCam = GetComponentInChildren<CameraInterface>();
        MilitaryManager.RegisterUnit(this);

    }

    void Update()
    {

        if (dead) return;
        UpdateCharacter();

        //Follow();
        Movement();
        Detection();
        ActiveTarget();
    }

    [PunRPC]
    public override void RPCDie(int characterId, float damage, Vector3 direction, int limbId)
    {
        if (dead) return;
        DamageInfo info = DamageInfo.GetInfo(this, characterId, damage, direction, limbId);
        if (dead) return;
        MilitaryManager.RemoveAssignment(this);
        //base.Die(info);
        base.RPCDie(characterId, damage, direction, limbId);
    }

    public void Call(Character character)
    {
        SetPath(Pathfind.GetPath(transform.position, character.transform.position));
        Debug.Log("call");
        /*
        if (follow)
        {
            follow = null;
            activePath = null;
        }
        else
        {
            follow = character;
        }
        */
    }

    /*
    public void Follow()
    {
        if (follow)
        {
            Debug.Log(followDelay % setFollowDelay);
            followDelay++;
            if (followDelay % setFollowDelay == 0)
            {
                SetPath(Pathfind.GetPath(transform.position, follow.transform.position));                
            }            

            if (follow.dead)
                Call(null);
        }
    }
    */

    public void ActiveTarget()
    {        
        if (!activeTarget) return;
        if (activeTarget.dead)
        {
            activeTarget = null;
            return;
        }

        if (repositionPath != null && repositionPath.nodes.Count > 0)
        {
            float speed = DistancedMovementSpeed(transform.position, repositionPath.nodes[repositionPath.nodes.Count - 1].transform.position, movementSpeed, 0.5f);
            FollowPath(repositionPath, speed, false);
        }
        if (detector.CanSee(transform.position + Vector3.up, activeTarget.transform.position + Vector3.up, activeTarget.gameObject, sightLayerMask))
        {
            AimAt(activeTarget.transform.position, 0.2f);
            Gun gun = heldItem.GetComponent<Gun>();
            if (gun && gun.ammo <= 0)            
                Reload();            
            else if (gun.CanUse())
                Attack();

            if ((activeTarget.transform.position - transform.position).magnitude < 5 && (repositionPath == null || repositionPath.nodes.Count < 1))
            {
                Move(-transform.forward, movementSpeed * 0.5f, false, true);
            }

            if ((activeTarget.transform.position - transform.position).magnitude < 1)
            {
                Vector3 direction = (transform.position - activeTarget.transform.position).normalized;
                PFPath newPath = new PFPath();
                PF_Node currentNode = Pathfind.GetNode(transform.position);
                for (int i = 0; i < 8; i++)
                {
                    PF_Node newNode = Pathfind.GetNearestAvailable(currentNode, currentNode.transform.position + direction, newPath.nodes);
                    currentNode = newNode;
                    newPath.nodes.Add(currentNode);
                }
                repositionPath = newPath;
            }
        }
        else
        {
            if (activePath != null && activePath.nodes.Count > 0)
                activePath.ReadjustPath(transform.position);
            activeTarget = null;
        }
    }

    public void Detection()
    {
        //detectionDelay++;
        //if (detectionDelay % setDetectionDelay != 0) return;
        if (activeTarget) return;

        //detector.UpdateDetections();
        if (!detector.updated)
            return;

        if (repositionPath != null && repositionPath.nodes.Count > 0) repositionPath = new PFPath();        
        Character nearest = null;
        float nearestDistance = 1000000;
        foreach (GameObject detection in detector.detections)
        {
            if (!detection) continue;
            Infected infected = detection.GetComponentInParent<Infected>();
            if (infected && !infected.dead && detector.CanSee(transform.position + Vector3.up, infected.transform.position + Vector3.up, infected.gameObject, sightLayerMask))
            {
                float distance = (transform.position - infected.transform.position).sqrMagnitude;
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearest = infected;
                }
            }        
        }
        activeTarget = nearest;
    }

    public void Movement()
    {        
        if (waitingDoor)
        {
            if (waitingDoor.opening)
                return;
            else
            {
                StartCoroutine(StopWaiting());
                return;
            }
        }
        if (activePath != null && activePath.nodes.Count > 0)
        {            
            float speed = movementSpeed;
            float speedCap = movementSpeed;

            if (animator.aiming)
                speedCap = 0.01f;

            speed = DistancedMovementSpeed(activePath.nodes[activePath.nodes.Count - 1].transform.position, transform.position, speedCap);
            FollowPath(activePath, speed, !animator.aiming);
            if (activePath.nodes.Count > 0)
                activePosition = activePath.nodes[0].transform.position;

            for (int i = 0; i < 1; i++)
            {
                if ((activePath.nodes.Count > i && activePath.nodes[i].blocked))
                {                    
                    //activePath.nodes = new List<PF_Node>();
                    Collider[] colliders = Physics.OverlapSphere(activePath.nodes[i].transform.position, 0.5f);
                    foreach(Collider collider in colliders)
                    {
                        BlastDoor door = collider.GetComponentInParent<BlastDoor>();
                        if (door)
                        {
                            if (!door.open && !door.opening)
                            {
                                door.Interact();
                                waitingDoor = door;
                            }
                            else
                            {
                                waitingDoor = door;
                            }   
                        }                        
                    }
                }
            }
        }
        else
        {
            if ((activePosition - transform.position).sqrMagnitude > 3*3)
            {
                SetPath(Pathfind.GetPath(Pathfind.GetNode(transform.position, activePosition), Pathfind.GetNode(activePosition)));
            }
        }
    }

    public virtual void SetPath(PFPath path)
    {
        activePath = path;        
    }

    IEnumerator StopWaiting()
    {
        if (waitingDoor != null)
        {
            yield return new WaitForSeconds(1.5f);
        }
        waitingDoor = null;
    }
}

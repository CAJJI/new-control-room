﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollJoint
{

    public GameObject connectedBody;
    public Vector3 anchor;
    public Vector3 axis;
    public Vector3 connectedAnchor;
    public Vector3 swingAxis;
    public SoftJointLimitSpring twistLimitSpring;
    public SoftJointLimit lowTwistLimit;
    public SoftJointLimit highTwistLimit;
    public SoftJointLimitSpring swingLimitSpring;
    public SoftJointLimit swing1Limit;
    public SoftJointLimit swing2Limit;
    public float projectionDistance;
    public float projectionAngle;
    public float breakForce;
    public float breakTorque;
    public float massScale;
    public float connectedMassScale;
}

public enum CharacterType
{
    None,
    Soldier,
    Infected,
    Player,
}

public class Limb : Damagable {

    public CharacterType characterType;
    public int id;
    RagdollJoint joint;
    public float damageMultiplier = 1;

    public Character character;
    new public Rigidbody rigidbody;    

    public void SaveJoint()
    {
        CharacterJoint cJoint = GetComponent<CharacterJoint>();
        if (!cJoint) return;
        joint = new RagdollJoint();
        joint.connectedBody= cJoint.connectedBody.gameObject;
        joint.anchor = cJoint.anchor;
        joint.axis = cJoint.axis;
        joint.connectedAnchor = cJoint.connectedAnchor;
        joint.swingAxis = cJoint.swingAxis;
        joint.twistLimitSpring = cJoint.twistLimitSpring;
        joint.lowTwistLimit = cJoint.lowTwistLimit;
        joint.highTwistLimit = cJoint.highTwistLimit;
        joint.swingLimitSpring = cJoint.swingLimitSpring;
        joint.swing1Limit = cJoint.swing1Limit;
        joint.swing2Limit = cJoint.swing2Limit;
        joint.projectionDistance = cJoint.projectionDistance;
        joint.projectionAngle = cJoint.projectionAngle;
        joint.breakForce = cJoint.breakForce;
        joint.breakTorque = cJoint.breakTorque;
        joint.massScale = cJoint.massScale;
        joint.connectedMassScale = cJoint.connectedMassScale;
    }

    public void DestroyJoint()
    {
        Destroy(GetComponent<CharacterJoint>());
        Destroy(GetComponent<Rigidbody>());
    }

    public void CreateJoint()
    {
        if (!rigidbody)
            rigidbody = gameObject.AddComponent<Rigidbody>();
        if (joint == null) return;
        CharacterJoint cJoint = gameObject.AddComponent<CharacterJoint>();
        if (!joint.connectedBody.GetComponent<Rigidbody>()) joint.connectedBody.AddComponent<Rigidbody>();
        cJoint.connectedBody = joint.connectedBody.GetComponent<Rigidbody>();
        cJoint.anchor = joint.anchor;
        cJoint.axis = joint.axis;
        cJoint.connectedAnchor = joint.connectedAnchor;
        cJoint.swingAxis = joint.swingAxis;
        cJoint.twistLimitSpring = joint.twistLimitSpring;
        cJoint.lowTwistLimit = joint.lowTwistLimit;
        cJoint.highTwistLimit = joint.highTwistLimit;
        cJoint.swingLimitSpring = joint.swingLimitSpring;
        cJoint.swing1Limit = joint.swing1Limit;
        cJoint.swing2Limit = joint.swing2Limit;
        cJoint.projectionDistance = joint.projectionDistance;
        cJoint.projectionAngle = joint.projectionAngle;
        cJoint.breakForce = joint.breakForce;
        cJoint.breakTorque = joint.breakTorque;
        cJoint.massScale = joint.massScale;
        cJoint.connectedMassScale = joint.connectedMassScale;
        cJoint.enableProjection = true;
    }

    public void SetCharacter(Character character)
    {
        id = character.limbs.Count;
        this.character = character;
        rigidbody = GetComponent<Rigidbody>();
        if (character.GetComponent<Infected>())
            characterType = CharacterType.Infected;

    }

    public override void Damage(DamageInfo info)
    {
        DamageInfo newInfo = DamageInfo.Copy(info);
        newInfo.damage *= damageMultiplier;
        newInfo.limb = this;
        character.Damage(newInfo);
    }

    public IEnumerator Disable(float afterSeconds)
    {
        yield return new WaitForSeconds(afterSeconds);
        Destroy(GetComponent<CharacterJoint>());
        Destroy(rigidbody);                
        //Destroy(GetComponent<Collider>());
    }
}

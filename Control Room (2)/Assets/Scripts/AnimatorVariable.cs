﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorVariable
{
    public Animator animator;
    public string variableName;
    public float resetTime;   

    public void Update()
    {
        if (resetTime < 2)
        {
            resetTime++;
            if (resetTime >= 2)
                Reset();
        }
    }

    public virtual void Reset()
    {

    }

    public AnimatorVariable(Animator animator, string variableName)
    {
        this.animator = animator;
        this.variableName = variableName;
    }
}

public class AnimatorBool : AnimatorVariable
{
    public AnimatorBool(Animator animator, string variableName) : base(animator, variableName)
    {
    }
    public bool state { get { return animator.GetBool(variableName); } set { animator.SetBool(variableName, value); resetTime = 0; } }
    public override void Reset()
    {
        animator.SetBool(variableName, false);
    }
    public static implicit operator bool(AnimatorBool animatorBool)
    {
        return animatorBool.state;
    }
}

public class AnimatorFloat : AnimatorVariable
{
    public AnimatorFloat(Animator animator, string variableName) : base(animator, variableName)
    {
    }
    public float value { get { return animator.GetFloat(variableName); } set { animator.SetFloat(variableName, value); resetTime = 0; } }
    public override void Reset()
    {
        animator.SetFloat(variableName, 0);
    }
    public static implicit operator float(AnimatorFloat animatorFloat)
    {
        return animatorFloat.value;
    }
}

public class AnimatorInt : AnimatorVariable {
    public AnimatorInt(Animator animator, string variableName) : base(animator, variableName) {
    }
    public int value { get { return animator.GetInteger(variableName); } set { animator.SetInteger(variableName, value); resetTime = 0; } }
    public override void Reset() {
        animator.SetInteger(variableName, 0);
    }
    public static implicit operator int(AnimatorInt animatorInt) {
        return animatorInt.value;
    }
}

public class AnimatorVector2 : AnimatorVariable
{
    public AnimatorVector2(Animator animator, string variableName) : base(animator, variableName)
    {
    }
    public Vector2 value { get { return new Vector2(animator.GetFloat(variableName + "X"), animator.GetFloat(variableName + "Y")); } set { animator.SetFloat(variableName + "X", value.x); animator.SetFloat(variableName + "Y", value.y); resetTime = 0; } }
    public override void Reset()
    {
        animator.SetFloat(variableName + "X", 0); animator.SetFloat(variableName + "Y", 0);
    }
    public static implicit operator Vector3(AnimatorVector2 animatorVector2)
    {
        return animatorVector2.value;
    }
}

public class AnimatorTrigger : AnimatorVariable
{
    public AnimatorTrigger(Animator animator, string variableName) : base(animator, variableName)
    {
    }    
    public override void Reset()
    {
        //animator.ResetTrigger(variableName);
    }
    public void Trigger()
    {
        animator.SetTrigger(variableName);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : Item {

    public GunType type;

    [PunRPC]
    public override void RPCInteract(int characterId)
    {
        Character character = Manager.Instance.GetBehaviour<Character>(characterId);

        GunDef def = GlobalManager.GameDef.GetGunDef(type);
        if (character.GetAmmo(type).amount + def.clipSize > def.maxAmmo)
        {
            Manager.CreateAudio(transform.position, GlobalManager.GameDef.sounds.pickUpAmmo);
            return;
        }
        character.GetAmmo(type).amount += def.clipSize;
        Manager.CreateAudio(transform.position, GlobalManager.GameDef.sounds.pickUpAmmo);
        Destroy(gameObject);        
    }
}

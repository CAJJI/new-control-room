﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLabel : MultiplayerBehaviour {

    public Player player;
    public TextMesh textMesh;

    public void AssignPlayer(Player player, Color color)
    {
        Vector3 colorVector = new Vector3(color.r, color.g, color.b);
        if (PhotonNetwork.inRoom)
            photonView.RPC("RPCAssignPlayer", PhotonTargets.AllBuffered, player.id, colorVector);
        else
            RPCAssignPlayer(player.id, colorVector);
    }

    [PunRPC]
    public void RPCAssignPlayer(int id, Vector3 color)
    {
        Player player = Manager.Instance.GetBehaviour<Player>(id);
        this.player = player;        
        if (player.photonView.isMine) Destroy(gameObject);
        textMesh.text = player.photonView.owner.NickName;
        textMesh.color = new Color(color.x, color.y, color.z, 0.7f);
        transform.SetParent(player.transform);
        transform.localPosition = Vector3.up * 1.8f;
    }

    private void Update()
    {
        transform.rotation = Quaternion.LookRotation((transform.position - Camera.main.transform.position));    
        if (player.dead) Destroy(gameObject);
    }
}
